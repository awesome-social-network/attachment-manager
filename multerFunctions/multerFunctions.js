
module.exports = {
/**
 * Check File Type
 * @param file
 * @param cb
 * @return {*}
 */
 checkFileType: function( file, cb ){
	// Allowed ext
	const filetypes = /jpeg|jpg|png|gif/;
	// Check mime
	console.log(file);
	const mimetype = filetypes.test( file.mimetype );
	console.log(mimetype);
	if( mimetype){
		return cb( null, true );
	} else {
		console.log("faux");	
		return cb(new Error('The file is not an valid img'),false);
	}
}
}
