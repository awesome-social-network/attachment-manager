FROM node:11-alpine
WORKDIR /app
COPY package.json /app
COPY swagger.json /app
RUN npm install
COPY . /app
CMD node controller/controller.js
EXPOSE 8081