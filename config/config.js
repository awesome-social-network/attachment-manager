require('dotenv').config({path:'../.env'})
module.exports = {
    express: require('express'),
    AWS: require("aws-sdk"),
    apiVersion: process.env.API_VERSION,
    bucket : process.env.BUCKET,
    multer: require('multer'),
    multerS3: require('multer-s3'),
    session : require('express-session'),
    swaggerUi : require('swagger-ui-express'),
	keycloak: require('keycloak-connect')
}