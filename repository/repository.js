const global = require('../config/config')
const SUC = {'result':'ok'}
const ERR = {'result':'error'}
const ep = new global.AWS.Endpoint(process.env.AWS_ENDPOINT)


module.exports = {
    getImg: function(id){
        return new Promise((resolve, reject) => {
            const objectParams = {Bucket: global.bucket, Key: id}
            var getImgPromise = new global.AWS.S3({apiVersion: global.apiVersion, endpoint: ep})
            .getObject(objectParams).promise();
            getImgPromise.then(img =>{
                resolve(JSON.stringify(img, null, 4))
            })
        })
    },

    deleteImg: function(id){
        return new Promise((resolve, reject) => {
            const objectParams = {Bucket: global.bucket, Key: id}
            var delImgPromise = new global.AWS.S3({apiVersion: global.apiVersion, endpoint: ep})
            .deleteObject(objectParams).promise();
            delImgPromise.then(resolve(SUC))
        })
    },
}