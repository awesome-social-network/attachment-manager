const global = require('../config/config')
const keycloakConfig = require('../config/keycloak')
const service = require('../service/service.js');
const uuidv4 = require('uuid/v4');
const cors = require("cors");
const multerF = require("../multerFunctions/multerFunctions")
const memoryStore = new global.session.MemoryStore();
const keycloak = new global.keycloak({ store: memoryStore },keycloakConfig.kcConfig)
const app = global.express()

var uuid ='';
var corsOptions = {
    origin: 'http://social-network.vitisb.fr',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

if(process.env.NODE_ENV === 'production') {
    app.use(cors(corsOptions))
} else {
    app.use(cors())
}

app.use(global.session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));
app.use(keycloak.middleware())
const ep = new global.AWS.Endpoint(process.env.AWS_ENDPOINT)
const s3 =  new global.AWS.S3({apiVersion: process.env.API_VERSION, endpoint: ep})

const uuidRegex = '/:id([0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12})'
const swaggerDocument = require('../swagger.json');

var upload = global.multer({
    storage: global.multerS3({
      s3: s3,
      bucket: global.bucket,
      metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
      },
      key: function (req, file, cb) {
        cb(null, uuid)
      }
    }),
    limits:{
      fileSize: 2048 * 2048,
      files: 1
    },
    fileFilter: function( req, file, cb ){
      multerF.checkFileType( file, cb );
    }
  })
app.listen(process.env.PORT, function () {
    console.log('server listening on port '+process.env.PORT)
})

app.get('/images'+ uuidRegex + '?',keycloak.protect(), function(req,res) {
    service.getImg(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error))
})

app.post('/images',function(req,res){
  uuid = uuidv4();
  upload.single('img')(req, res, function (err) {
    console.log('files '+req.files)
    console.log('file '+req.file)
    console.log('err '+err)
    if (err instanceof global.multer.MulterError) {
      res.status(413).send(err)
    }
    else if(err != undefined){
      console.log(err.constructor)
      err += ' '
      if(err.indexOf('The file is not an valid img') != -1){
        //check for another code status
        res.status(406).send(err)
      }
    }
    else{
      console.log('SUCCESS')
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
      res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
      res.setHeader('Access-Control-Allow-Credentials', true); // If needed
      res.setHeader('uuid',uuid)
      res.status(201).send(uuid)
    } 
  })
})

app.delete('/images' + uuidRegex,keycloak.protect(), function(req, res) {
    service.deleteImg(req.params.id)
    .then((result) => res.status(200).send(result))
    .catch((error) => res.status(500).send(error))
})

app.get('/logoff', keycloak.protect(), (req, res) => {
  console.log('logout clicked');
  // Due to CORS setup on the keycloak server, we can't call the /logout
  // route directly through the Angular client. We need to pass the URL
  // from the server (with CORS headers) and then call that URL from the client.
  // Reference: https://stackoverflow.com/questions/49835830/res-redirect-cors-not-working-in-mean-app
  res.send('http://localhost:'+process.env.PORT+ '/logout');
});

app.use('/api-docs', global.swaggerUi.serve, global.swaggerUi.setup(swaggerDocument));


