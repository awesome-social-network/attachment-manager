const global = require('../config/config')
var repository = require('../repository/repository')

module.exports = {
    getImg: function(id){
        return new Promise((resolve, reject) => {
            repository.getImg(id)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    },
    deleteImg: function(id){
        return new Promise((resolve, reject) => {     
            repository.deleteImg(id)
            .then((result) => resolve(result))
            .catch((error) => reject(error))
        })
    }
}